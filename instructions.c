#include "instructions.h"

//The array storing variables X0 through X30
long variables[32];

//The conditions that get set by SUBS and SUBIS
struct conditions conds;

char *stack;

char *mainMem;

int stackSize;

int mainMemSize;

void hexdump(FILE *f, char *start, size_t size);
void deInit();
void setConds(long long num);

//Takes in an instruction and returns a struct which represents and r type
struct R getRType(long instruction)
{
    struct R toReturn;
    toReturn.rm = instruction >> 16 & 0x1f;
    toReturn.shamt = instruction >> 10 & 0x3f;
    toReturn.rn = instruction >> 5 & 0x1f;
    toReturn.rd = instruction & 0x1f;
    return toReturn;
}

struct I getIType(long instruction)
{
    struct I toReturn;
    toReturn.immediate = instruction >> 10 & 0xfff;
    toReturn.rn = instruction >> 5 & 0x1f;
    toReturn.rd = instruction & 0x1f;
    return toReturn;
}

struct D getDType(long instruction)
{
    struct D toReturn;
    toReturn.DT_ADDRESS = instruction >> 12 & 0x1ff;
    toReturn.op = instruction >> 10 & 0x3;
    toReturn.rn = instruction >> 5 & 0x1f;
    toReturn.rt = instruction & 0x1f;
    return toReturn;
}

struct CB getCBType(long instructions)
{
    struct CB toReturn;
    toReturn.branchAddress = instructions >> 5 & 0x7ffff;
    toReturn.rt = instructions & 0x1f;
    return toReturn;
}

// Cycle logic
void incrementCycles(int regDest, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    for (int i = 0; i < sizeof(stagedRegs) / sizeof(stagedRegs[0]) - 1; i++)
    {
        if (regDest == stagedRegs[i])
        {
            *dataHazards = *dataHazards + 1;
            if (i < 3)
                *dataHazardsf = *dataHazardsf + 1;
            ;
        }
    }

    for (int i = 0; i < sizeof(stagedRegs) / sizeof(stagedRegs[0]) - 2; i++)
        stagedRegs[i + 1] = i;
    stagedRegs[0] = regDest;
}

// Control Hazard logic
void incrementControl(int index, int *stagedI, int *controlHazards, int newIndex)
{
    for (int i = 0; i <= 3; i++) // IF, REG, MEM -- can use branch aftre MEM stage
        if (index == stagedI[i])
            *controlHazards = *controlHazards + 1;

    for (int i = 0; i < sizeof(stagedI) / sizeof(stagedI[0]) - 2; i++)
    { // print to test if this has four iterations
        stagedI[i + 1] = i;
        printf("Increment Control with index = %d, at iteration in loop with i = %d", index, i);
    }
    stagedI[0] = newIndex;
}

void add(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{

    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] + variables[parsed.rm];
    //printf("Parsed.rd: %ld Parsed.rn %ld Parsed.rm %ld", variables[parsed.rd], variables[parsed.rn], variables[parsed.rm]);
    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void addi(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct I parsed = getIType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] + parsed.immediate;

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void and (long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    // If rn
    variables[parsed.rd] = variables[parsed.rn] & variables[parsed.rd];

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void andi(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct I parsed = getIType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] & parsed.immediate;

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}
//UNTESTED
int b(long instruction, int *index, int *stagedI, int *controlHazards)
{
    long branchAddr = instruction & 0x003ffffff;
    if (branchAddr & 0x2000000)
    {
        branchAddr = branchAddr | 0xfffffffffC000000;
    }
    int newIndex = *index + (branchAddr * 4) - 4;
    incrementControl(*index, stagedI, controlHazards, newIndex);
    *index = newIndex;
}
//UNTESTED
int bCond(long instruction, int *index, int *stagedI, int *controlHazards)
{
    struct CB parsed = getCBType(instruction);
    if ((conds.eq && parsed.rt == 0) ||
        (conds.ge && (parsed.rt == 0xa || parsed.rt == 2 || parsed.rt == 5)) ||
        (conds.gt && (parsed.rt == 0xc || parsed.rt == 8)) ||
        (conds.le && (parsed.rt == 0xd || parsed.rt == 9)) ||
        (conds.lt && (parsed.rt == 3 || parsed.rt == 0xb || parsed.rt == 4)) ||
        (!conds.eq && parsed.rt == 1))
    {
        if (parsed.branchAddress & 0x40000)
        {
            parsed.branchAddress = parsed.branchAddress | 0xffffffffff80000;
        }
        //printf("Parsed cond br addr: %lx\n", parsed.branchAddress);

        int newIndex = *index + (parsed.branchAddress * 4) - 4;
        incrementControl(*index, stagedI, controlHazards, newIndex);
        *index = newIndex;
    }
}
//UNTESTED
int bl(long instruction, int *index, int *stagedI, int *controlHazards)
{
    variables[30] = (*index + 4) / 4;

    long branchAddr = instruction & 0x3ffffff;
    if (branchAddr & 0x2000000)
    {
        branchAddr = branchAddr | 0xfffffffffC000000;
    }
    int newIndex = *index + (branchAddr * 4) - 4;
    incrementControl(*index, stagedI, controlHazards, newIndex);
    *index = newIndex;
}
//UNTESTED
int br(long instruction, int *index, int *stagedI, int *controlHazards)
{
    //printf("X30 contains: %ld \n", variables[30]);
    struct R parsed = getRType(instruction);
    int newIndex = (variables[30] * 4) - 4; // could be something other than rn, the instruction set cheat says wrong stuff
    incrementControl(*index, stagedI, controlHazards, newIndex);
    //printf("intstruction: %ld parsed id was: %ld and newIndex was %d \n", instruction, variables[parsed.rd], newIndex);
    *index = newIndex;
    //halt(0);
}
//UNTESTED
int cbnz(long instruction, int *index, int *stagedI, int *controlHazards, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct CB parsed = getCBType(instruction);
    if (variables[parsed.rt])
    {
        if (parsed.branchAddress & 0x40000)
        {
            parsed.branchAddress = parsed.branchAddress | 0xfffffffffff80000;
        }
        //printf("Here's the branch address %lx\n", parsed.branchAddress);
        int newIndex = *index + (parsed.branchAddress * 4) - 4;
        incrementControl(*index, stagedI, controlHazards, newIndex);
        *index = newIndex;
    }
    else
    {
        //printf("cbnz -- variables[parsed.rt] null\n");
    }

    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}
//UNTESTED
int cbz(long instruction, int *index, int *stagedI, int *controlHazards, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct CB parsed = getCBType(instruction);
    if (!variables[parsed.rt])
    {
        if (parsed.branchAddress & 0x40000)
        {
            parsed.branchAddress = parsed.branchAddress | 0xfffffffffff80000;
        }
        //printf("Here's the branch address %lx\n", parsed.branchAddress);
        int newIndex = *index + (parsed.branchAddress * 4) - 4;
        incrementControl(*index, stagedI, controlHazards, newIndex);
        *index = newIndex;
    }
    else
    {
        //printf("cbz -- variables[parsed.rt] true \n");
    }
    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}

//MAYBE NOT FULLY IMPLEMENTED!!
void dump(long instruction)
{
    //Print registers
    printf("Registers: \n");
    int x;
    char *label = "      ";
    for (x = 0; x < 32; x++)
    {
        if (x == 16)
            label = "(IP0) ";
        else if (x == 17)
            label = "(IP1) ";
        else if (x == 28)
            label = " (SP) ";
        else if (x == 29)
            label = " (FP) ";
        else if (x == 30)
            label = " (LR) ";
        else if (x == 31)
            label = "(XZR) ";
        else
            label = "      ";
        printf("%s X%d: 0x%lx (%d)\n", label, x, (long unsigned)variables[x], (int)variables[x]);
    }

    //Print Stack
    printf("Stack:\n");
    hexdump(stdout, stack, stackSize);

    //Print Main Memory
    printf("Main Memory:\n");
    hexdump(stdout, mainMem, mainMemSize);

    //Program??

    //Extra??
}

void eor(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] ^ variables[parsed.rm];

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void eori(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct I parsed = getIType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] ^ parsed.immediate;

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void halt(long instruction)
{
    dump(instruction);
    deInit();
    exit(0);
}

void ldur(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct D parsed = getDType(instruction);
    if (parsed.rt == 31)
        return;
    variables[parsed.rt] = mainMem[variables[parsed.rn] + parsed.DT_ADDRESS];

    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}

void ldurb(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct D parsed = getDType(instruction);
    if (parsed.rt == 31)
        return;
    variables[parsed.rt] = mainMem[variables[parsed.rn] + parsed.DT_ADDRESS];

    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}

void ldurh(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct D parsed = getDType(instruction);
    if (parsed.rt == 31)
        return;
    variables[parsed.rt] = mainMem[variables[parsed.rn] + parsed.DT_ADDRESS];

    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}

void ldursw(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct D parsed = getDType(instruction);
    if (parsed.rt == 31)
        return;
    variables[parsed.rt] = mainMem[variables[parsed.rn] + parsed.DT_ADDRESS];

    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}

void lsl(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] << parsed.shamt;

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void lsr(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] >> parsed.shamt;

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void mul(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] * variables[parsed.rm];

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void orr(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] | variables[parsed.rm];

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void orri(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct I parsed = getIType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] | parsed.immediate;

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void prnl(long instruction)
{
    //printf("\n");
}

void prnt(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    //printf("X%d: 0x%lx %d\n", parsed.rd, (long unsigned)variables[parsed.rd], (int)variables[parsed.rd]);

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}
//NOT SURE IF CORRECTLY IMPLEMENTED, MAY REQUIRE
void sdiv(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] / variables[parsed.rm];

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}
//MIGHT BE WRONG IMPLEMENTATION
void smulh(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] * variables[parsed.rm];

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void stur(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct D parsed = getDType(instruction);
    mainMem[parsed.DT_ADDRESS + variables[parsed.rn]] = variables[parsed.rt];
    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}

void sturb(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct D parsed = getDType(instruction);
    mainMem[parsed.DT_ADDRESS + variables[parsed.rn]] = variables[parsed.rt];

    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}

void sturh(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct D parsed = getDType(instruction);
    mainMem[parsed.DT_ADDRESS + variables[parsed.rn]] = variables[parsed.rt];

    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}

void sturw(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct D parsed = getDType(instruction);
    mainMem[parsed.DT_ADDRESS + variables[parsed.rn]] = variables[parsed.rt];

    incrementCycles(parsed.rt, stagedRegs, dataHazards, dataHazardsf);
}

void sub(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] - variables[parsed.rm];

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void subi(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct I parsed = getIType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] - parsed.immediate;

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void subis(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct I parsed = getIType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] - parsed.immediate;
    setConds(variables[parsed.rd]);

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void subs(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] - variables[parsed.rm];
    setConds(variables[parsed.rd]);

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}
//NOT SURE IF CORRECT IMPLEMENTATION
void udiv(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] / variables[parsed.rm];

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

//NOT SURE IF CORRECT IMPLEMENTATION
void umulh(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf)
{
    struct R parsed = getRType(instruction);
    if (parsed.rd == 31)
        return;
    variables[parsed.rd] = variables[parsed.rn] * variables[parsed.rm];

    incrementCycles(parsed.rd, stagedRegs, dataHazards, dataHazardsf);
}

void setConds(long long num)
{
    conds.eq = num == 0;
    conds.ge = num >= 0;
    conds.gt = num > 0;
    conds.le = num <= 0;
    conds.lt = num < 0;
}

void printVars()
{
    int x, i;
    for (x = 0; x < 32; x++)
    {
        for (i = 0; i < 32; i++)
        {
            printf("%d", !!((variables[x] << i) & 0x80000000));
        }
        printf("\n");
    }
}

void init(int sizeMainMem, int sizeStack)
{
    if (sizeMainMem == 0)
        sizeMainMem = 4096;
    if (sizeStack == 0)
        sizeStack = 512;
    int x;
    for (x = 0; x < 32; x++)
    {
        variables[x] = 0;
    }
    mainMem = malloc(sizeMainMem);
    stack = malloc(sizeStack);
    for (x = 0; x < sizeMainMem; x++)
    {
        mainMem[x] = 0;
    }
    for (x = 0; x < sizeStack; x++)
    {
        stack[x] = 0;
    }
    mainMemSize = sizeMainMem;
    stackSize = sizeStack;
    variables[28] = stackSize;
    variables[29] = mainMemSize;
    conds.eq = 0;
    conds.ge = 0;
    conds.gt = 0;
    conds.le = 0;
    conds.lt = 0;
}

void deInit()
{
    free(mainMem);
    free(stack);
}

char printable_char(char c)
{
    return isprint(c) ? c : '.';
}

void hexdump(FILE *f, char *start, size_t size)
{
    size_t i;

    for (i = 0; i < size - (size % 16); i += 16)
    {
        fprintf(f,
                "%08x "
                " %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx "
                " %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx %02hhx "
                " |%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c|\n",
                (int32_t)i,
                start[i + 0], start[i + 1], start[i + 2], start[i + 3],
                start[i + 4], start[i + 5], start[i + 6], start[i + 7],
                start[i + 8], start[i + 9], start[i + 10], start[i + 11],
                start[i + 12], start[i + 13], start[i + 14], start[i + 15],
                printable_char(start[i + 0]), printable_char(start[i + 1]),
                printable_char(start[i + 2]), printable_char(start[i + 3]),
                printable_char(start[i + 4]), printable_char(start[i + 5]),
                printable_char(start[i + 6]), printable_char(start[i + 7]),
                printable_char(start[i + 8]), printable_char(start[i + 9]),
                printable_char(start[i + 10]), printable_char(start[i + 11]),
                printable_char(start[i + 12]), printable_char(start[i + 13]),
                printable_char(start[i + 14]), printable_char(start[i + 15]));
    }
    fprintf(f, "%08x\n", (int32_t)size);
}

void printConds()
{
    printf("Conditions: eq: %d ge: %d gt: %d le: %d lt: %d\n", conds.eq, conds.ge, conds.gt, conds.le, conds.lt);
}
