#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <endian.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <limits.h>
#include <sys/time.h>
#include <assert.h>
struct R{
    int rm;
    int shamt;
    int rn;
    int rd;
};

struct I{
    long immediate;
    int rn;
    int rd;
};

struct D{
    int DT_ADDRESS;
    char op;
    int rn;
    int rt;
};

struct CB{
    long branchAddress;
    int rt;
};

struct conditions {
    char eq;
    char ge;
    char gt;
    char le;
    char lt;
};

struct R getRType(long instruction);

struct I getIType(long instruction);

struct CB getCBType(long instructions);

struct IW getIWType(long instructions);

void incrementCycles(int regDest, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void incrementControl(int index, int *stagedI, int *controlHazards, int newIndex);

void printVars();

void init();

void add(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void addi(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void and(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void andi(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

int b(long instruction, int *index, int *stagedI, int *controlHazards); // modified branches to take in pointer to index

int bCond(long instruction, int *index, int *stagedI, int *controlHazards);

int bl(long instruction, int *index, int *stagedI, int *controlHazards);

int br(long instruction, int *index, int *stagedI, int *controlHazards);

int cbnz(long instruction, int *index, int *stagedRegs, int *dataHazards, int *dataHazardsf, int *stagedI, int *controlHazards);

int cbz(long instruction, int *index, int *stagedRegs, int *dataHazards, int *dataHazardsf, int *stagedI, int *controlHazards);

void dump(long instruction);

void eor(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void eori(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void halt(long instruction);

void ldur(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void ldurb(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void ldurh(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void ldursw(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void lsl(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void lsr(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void mul(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void orr(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void orri(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void prnl(long instruction);

void prnt(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void sdiv(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void smulh(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void stur(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void sturb(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void sturh(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void sturw(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void sub(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void subi(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void subis(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void subs(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void udiv(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void umulh(long instruction, int *stagedRegs, int *dataHazards, int *dataHazardsf);

void deInit();

void printConds();

