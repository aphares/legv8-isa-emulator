#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <endian.h>
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <limits.h>
#include <sys/time.h>
#include <assert.h>
#include "instructions.h"

// void printOpcodeUtil(char *buffer) {
//             // Printing bits for debugging
//         //printf("Bit 1: %d\n", !!((buffer[i] << 0) & 0x80));
//         //printf("Bit 2: %d\n", !!((buffer[i] << 1) & 0x80));
//         //printf("Bit 3: %d\n", !!((buffer[i] << 2) & 0x80));
//         //printf("Bit 4: %d\n", !!((buffer[i] << 3) & 0x80));
//         //printf("Bit 5: %d\n", !!((buffer[i] << 4) & 0x80));
//         //printf("Bit 6: %d\n", !!((buffer[i] << 5) & 0x80));
//         //printf("Bit 7: %d\n", !!((buffer[i] << 6) & 0x80));
//         //printf("Bit 8: %d\n", !!((buffer[i] << 7) & 0x80));
//         //printf("Bit 9: %d\n", !!((buffer[i + 1] << 0) & 0x80));
//         //printf("Bit 10: %d\n", !!((buffer[i + 1] << 1) & 0x80));
//         //printf("Bit 11: %d\n", !!((buffer[i + 1] << 2) & 0x80));
// }

int getNextBit(int *shift, int *curBit, int i)
{
    // *shift++
    //*curBit = !!((buffer[i] << shift) & 0x80))
    return *curBit;
}

char *fillBuffer(char *fileName, size_t *size)
{
    FILE *ptr;
    ptr = fopen(fileName, "rb"); // r for read, b for binary
    fseek(ptr, 0L, SEEK_END);    // go to end of file
    size_t sz = ftell(ptr);
    fseek(ptr, 0L, SEEK_SET); // return to beginning of file
    char *buffer = malloc(sz);
    fread(buffer, sz, 1, ptr);
    fclose(ptr);
    *size = sz;
    return buffer;
}

void printBufferBinary(char *buffer, size_t size)
{
    int x, i;
    for (i = 0; i < size; i++)
    {
        for (x = 0; x < 8; x++)
        {
            //printf("%d", !!((buffer[i] << x) & 0x80));
        }
        //printf("\n");
    }
}

void printStatistics(int uCycles, int dataHazards, int dataHazardsf, int controlHazards)
{
    // no branch prediction for any required statistic. Need unpipelined, pipelined and no bypassing, pipelined and bypassing, and count of control and data hazards
    // bypassing: forwarding (e.g. for ADD X19, X0, X1 | SUB X2, X19, X3 -- can forward after 3rd state (execution  stage))
    printf("Number of cycles on an unpipelined implementation of the ISA was: %d cycles \n", uCycles);
    printf("Number of cycles on a 5-stage-pipelined implementation of the ISA without bypassing or branch prediction was: %d cycles \n", uCycles + dataHazards);
    printf("Number of data hazards incurred by the program in the 5-stage pipeline was %d hazards: \n", dataHazards);
    printf("Number of control hazards incurred by the program in the 5-stage pipeline was: %d\n", controlHazards);
    printf("Number of cycles on a 5-stage-pipelined implementation of the ISA with bypassing but no branch prediction was: %d \n", uCycles + dataHazardsf);
}

int main(int argc, char *argv[])
{
    size_t sz;
    char *buffer = fillBuffer(argv[1], &sz);
    int i = 0;                                // below: 40 is abitrary, needed value no other registers will equal.
    int stagedRegs[5] = {-1, -1, -1, -1, -1}; // index 1 is at IF stage, index 2 at REG, index 3 at MEM, index 4 at EXE, index 5 at WB. For forwarding, cannot use any registers @i = 1 || 2
    int stagedI[5] = {-1, -1, -1, -1, -1};
    int dataHazards = 0; // incremented in instructions.c
    int dataHazardsf = 0;
    int controlHazards = 0;
    int unpipelinedCycles = 0; // until piazza gives more info I'm assuming each instruction uses 5 cycles, for the general 5 stages: instruction fetch, register fetch, execute, memoy, write back

    init(0, 0);
    //printf("First line hsas instruction code = %d\n", (unsigned char)buffer[i]);
    while (i < sz)
    {
        //Bit shift 4 buffer characters then add them together to create an instruction
        long buff0 = buffer[i] << 24 & 0xff000000;
        long buff1 = buffer[i + 1] << 16 & 0x00ff0000;
        long buff2 = buffer[i + 2] << 8 & 0x0000ff00;
        long buff3 = buffer[i + 3] & 0x000000ff;
        long instruction = buff0 + buff1 + buff2 + buff3;
        //printf("Instruction: %lx\n", instruction);
        //  Check if six-opcode B or BL (check each bit)
        if ((!(!!((buffer[i]) & 0x80)))         // 0
            && (!(!!((buffer[i] << 1) & 0x80))) // 0
            && (!(!!((buffer[i] << 2) & 0x80))) // 0
            && (!!((buffer[i] << 3) & 0x80))    // 1
            && (!(!!((buffer[i] << 4) & 0x80))) // 0
            && (!!((buffer[i] << 5) & 0x80)))   // 1
        //if((buffer[i] & 0b000100) && (buffer[i] & 0b000001) && !(buffer[i] | 0b111010))
        {
            b(instruction, &i, stagedI, &controlHazards);
            //printf("B: %lx -- i = %d\n", instruction, i);
        }
        else if ((!!((buffer[i]) & 0x80))            // 1
                 && (!(!!((buffer[i] << 1) & 0x80))) // 0
                 && (!(!!((buffer[i] << 2) & 0x80))) // 0
                 && (!!((buffer[i] << 3) & 0x80))    // 1
                 && (!(!!((buffer[i] << 4) & 0x80))) // 0
                 && (!!((buffer[i] << 5) & 0x80)))   // 1
        {
            bl(instruction, &i, stagedI, &controlHazards);
            //printf("BL: %lx -- i = %d\n", instruction, i);
        }
        else
        {

            switch ((unsigned char)buffer[i])
            {

            case 56: // STURB LDURB
                if (!!((buffer[i + 1] << 1) & 0x80))
                {
                    ldurb(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("LDURB: %lx -- i = %d\n", instruction, i);
                }
                else
                {
                    sturb(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("STURB: %lx -- i = %d\n", instruction, i);
                }
                break;
            case 84: // B.cond
                bCond(instruction, &i, stagedI, &controlHazards);
                //printf("B.cond: %lx -- i = %d\n", instruction, i);
                break;
            case 120: // STURH, LDURH
                if (!!((buffer[i + 1] << 1) & 0x80))
                {
                    ldurh(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("LDURH: %lx -- i = %d\n", instruction, i);
                }
                else
                {
                    sturh(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("STURH: %lx -- i = %d\n", instruction, i);
                }
                break;
                break;
            case 129: // AND
                and(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("AND: %lx -- i = %d\n", instruction, i);
                break;
            case 139: // ADD
                //printf("ADD: %lx -- i = %d\n", instruction, i);
                add(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("ADD: %lx -- i = %d\n", instruction, i);
                break;
            case 145: // ADDI
                addi(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("ADDI: %lx -- i = %d\n", instruction, i);
                break;
            case 154: // SDIV, UDIV
                // TODO -- SDIV and UDIV are identical, and according to piazza: will need to check shamts (which is currently broken)
                if (1)
                {
                    sdiv(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("LDURB: %lx -- i = %d\n", instruction, i);
                }
                else
                {
                    udiv(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("STURB: %lx -- i = %d\n", instruction, i);
                }
                break;
                break;
            case 155: // MUL SMULH UMULH
                if (!!((buffer[i + 1] << 0) & 0x80))
                {
                    umulh(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("UMULH: %lx -- i = %d\n", instruction, i);
                }
                else if (!!((buffer[i + 1] << 1) & 0x80))
                {
                    smulh(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("SMULH: %lx -- i = %d\n", instruction, i);
                }
                else
                {
                    mul(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("MUL: %lx -- i = %d\n", instruction, i);
                }
                break;
            case 146: // ANDI
                andi(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("ANDI: %lx -- i = %d\n", instruction, i);
                break;
                break;
            case 181: // CBNZ
                cbnz(instruction, &i, stagedRegs, &dataHazards, &dataHazardsf, stagedI, &controlHazards);
                //printf("CBNZ: %lx -- i = %d\n", instruction, i);
                break;
                break;
            case 178: // ORRI
                orri(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("ORRI: %lx -- i = %d\n", instruction, i);
                break;
                break;
            case 180: // CBZ
                cbz(instruction, &i, stagedRegs, &dataHazards, &dataHazardsf, stagedI, &controlHazards);
                //printf("CBZ: %lx -- i = %d\n", instruction, i);
                break;
            case 184: // STURW LDURSW
                if (!!((buffer[i + 1] << 0) & 0x80))
                {
                    ldursw(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("LDURSW: %lx -- i = %d\n", instruction, i); //check
                }
                else
                {
                    sturw(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("STURW: %lx -- i = %d\n", instruction, i);
                }
                break;
            case 210: // EORI
                eori(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("EORI: %lx -- i = %d\n", instruction, i);
                break;
            case 211: // LSL LSR
                if (!!((buffer[i + 1] << 2) & 0x80))
                {
                    lsl(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("LSL: %lx -- i = %d\n", instruction, i);
                }
                else
                {
                    lsr(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("LSR: %lx -- i = %d\n", instruction, i);
                }
                break;
            case 209: // SUBI
                subi(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("SUBI: %lx -- i = %d\n", instruction, i);
                break;
            case 214: // BR
                br(instruction, &i, stagedI, &controlHazards);
                //printf("BR: %lx -- i = %d\n", instruction, i);
                break;
            case 241: // SUBIS
                subis(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("SUBIS: %lx -- i = %d\n", instruction, i);
                break;
            case 248: // LDUR STUR
                // printOpcodeUtil(buffer);
                if (!!((buffer[i + 1] << 1) & 0x80))
                {
                    ldur(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("LDUR: %lx -- i = %d\n", instruction, i);
                }
                else
                {
                    //printf("STUR: %lx -- i = %d\n", instruction, i);
                    stur(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                }
                break;
            case 255: // DUMP, HALT, PRNL, PRNT
                //printOpcodeUtil(buffer);
                if (!(!!((buffer[i + 1] << 0) & 0x80)))
                {
                    prnt(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                    //printf("PRNT: %lx -- i = %d\n", instruction, i);
                }
                else if (!(!!((buffer[i + 1] << 1) & 0x80)))
                {
                    prnl(instruction);
                    //printf("PRNL: %lx -- i = %d\n", instruction, i);
                }
                else if (!!((buffer[i + 1] << 1) & 0x80))
                {
                    halt(instruction);
                    //printf("HALT: %lx -- i = %d\n", instruction, i);
                }
                else
                {
                    dump(instruction);
                    //printf("DUMP: %lx -- i = %d\n", instruction, i);
                }
                break;
            case 203: // SUB
                sub(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("SUB: %lx -- i = %d\n", instruction, i);
                break;
            case 200: // EOR
                eor(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("EOR: %lx -- i = %d\n", instruction, i);
                break;
            case 235: // SUBS
                subs(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("SUBS: %lx -- i = %d\n", instruction, i);
                break;
            case 170: // ORR
                orr(instruction, stagedRegs, &dataHazards, &dataHazardsf);
                //printf("ORR: %lx -- i = %d\n", instruction, i);
                break;
            }
        }
        unpipelinedCycles += 5; // Assuming one cycle for each instruction
        i += 4;
    }
    printStatistics(unpipelinedCycles, dataHazards, dataHazardsf, controlHazards);
    free(buffer);
    halt(0);
    return 0;
    /*init(64,16); //Eric Tests, please leave for now
	addi(0x0000FC00);
	stur(0b11111000000000000000000000100000);
	dump(0);
	ldur(0b11111000010000000000000000100010);
	dump(0);
	prnt(0x00000002);
	prnt(0);
	deInit();
	//halt(0);
    return 0;*/
}
